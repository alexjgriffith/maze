# Maze -  A simple maze building utility

A library to produce and display mazes.


## Functions
#### Make
``` lua
make (width <int>, height <int>, x <int>, y <int>, x2 <int>, y2 <int>)
```

Initializes and carves a maze with a single exit. Note that `width`, `height`, `x` and `y` must all be odd.

Returns a [Map](#map) table.

``` lua
maze.make(5, 5, 3, 1, 3, 0)
```

``` lisp
(maze.make 5 5 3 1 3 0)
```

#### Find
``` lua
make (Maze<map>, max<int>)
```

Locate a part of the maze that is not a wall. It will make `max` attempts to select a random block that is not a wall.

Returns an `<int>`

``` lisp
(local maze (require "maze"))
(let [map (maze.make 5 5 3 1 3 0)]
  (maze.find map))
```

#### Show
``` lua
show (Maze<map>)
```

Draws the map, 
- walls are drawn as `[]` 
- a starting position (as identified by a 2 in map.data) is represented as `<>`
- A path (as identified by a 3 in map.data) is represented as `..`
    
``` lisp
(local maze (require "maze"))
(let [width 5 height 5]
(let [map   (maze.make width height 3 1 3 0)
  found (maze.find map)]
  ;; Update the map.data to reflect a valid starting position
  (tset map.data found 2)
  (maze.show map))


;; [][][]  []
;; []      []
;; []  [][][]
;; []  <>  []
;; [][][][][]
```

## Structures

#### Map
``` lisp
{data:[<int>...],entrance:[<int>,<int>],width:<int>,height:<int>}
```
