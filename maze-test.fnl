(local maze (require "maze"))

(let [width 5 height 5]
  (print "")
  (let [map (maze.make width height 3 1 3 0)
            found (maze.find map)]
    (tset map.data found 2)
    (maze.show map)))



