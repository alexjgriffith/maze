-- Maze generator in Lua
-- Joe Wingbermuehle 2013-10-06
-- Alexander Griffith 2019-04-19

function bound (x, y, width, height)
   local buffer = 0
   return (y < ( height - buffer - 1)) and (y > (buffer )) and
      (x < (width - buffer - 1)) and (x > (buffer))
end

function carve_aux(maze, width, height, x, y)
   local r = math.random(0, 3)
   maze[y * width + x] = 0
   for i = 0, 3 do
      local d = (i + r) % 4
      local dx = 0
      local dy = 0
      if d == 0 then
         dx = 1
      elseif d == 1 then
         dx = -1
      elseif d == 2 then
         dy = 1
      else
         dy = -1
      end
      local nx = x + dx
      local ny = y + dy
      local nx2 = nx + dx
      local ny2 = ny + dy
      if maze[ny * width + nx] == 1 then
         if maze[ny2 * width + nx2] == 1 then
            if bound(nx2, ny2, width, height) then 
               maze[ny * width + nx] = 0
               carve_aux(maze, width, height, nx2, ny2)
            end
         end
      end
   end
end

-- Make a maze width by height with an entrance at x y x2 y2
-- note x y have to be within the maze while x2 y2 is the
-- ajacent exit.
-- Output {entrance,width,height,data}
-- data is an array that represent indicies
-- 0 - nothing
-- 1 - wall
-- 2 - start
-- 3 - path
function make (width, height, x , y, x2,y2)
   -- The size of the maze (must be odd).
   if width==nil then width = 31 end
   if height==nil then height = 31 end
   if x==nil then x = 1 end
   if y==nil then y = 5 end
   if x2==nil then x = 0 end
   if y2==nil then y = 5 end

   -- Generate and display a random maze.
   local local_maze = {}
   for y = 0, height - 1 do
      for x = 0, width - 1 do
         local_maze[y * width + x] = 1
      end
   end
   carve_aux(local_maze,width,height,x,y)
   local_maze[width*y2 + x2] = 0
   local output ={}
   for k,v in pairs(local_maze) do
      output[k+1] = v
   end
   local entrance = {x,y}
   return {entrance=entrance,width=width,height=height, data=output}
end

-- Show a maze.
-- Example output
-- [][][]  []
-- []      []
-- []  [][][]
-- []  <>  []
-- [][][][][]
function show(maze)
   local height = maze.height
   local width = maze.width
   local maze = maze.data
   for y = 0, height - 1 do
      for x = 0, width - 1 do
         if maze[y * width + x +1] == 0 then
            io.write("  ")
         elseif maze[y * width + x +1] == 2 then
            io.write("<>")
         elseif maze[y * width + x +1] == 3 then
            io.write("..")            
         else
            io.write("[]")
         end
      end
      io.write("\n")
   end
end

-- Find an open slot.
function find(maze, max)
   if max ==nil then max=10 end
   local length = maze.height * maze.width
   local r = math.random(1, length + 1)
   if(maze.data[r] == 0) then
      return r
   elseif max==0 then
      return nil
   else
      return find(maze, max -1)
   end   
end

return {make=make,show=show,find=find}
